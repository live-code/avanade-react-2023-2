export interface Product {
  id: number;
  name: string;
  desc: string;
  cost: number;
}
