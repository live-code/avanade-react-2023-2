import React, { FunctionComponent, FunctionComponentElement, PropsWithChildren, Suspense, useState } from 'react';
/*import WidgetChart from './components/WidgetChart';
import WidgetProfile from './components/WidgetProfile';*/

const WidgetChart = React.lazy(() => import('./components/WidgetChart'));
const WidgetProfile = React.lazy(() => import('./components/WidgetProfile'));

// JSON DATA
const dashboard: { component: string, data: any}[] = [
  {
    component: 'profile',
    data: { title: 'Profile John Doe', color: 'red', children: 'SOME CONTENT HERE!' }
  },
  {
    component: 'chart',
    data: { id: 2, title: 'I am a chart!', data: [10, 20, 30] }
  },
  {
    component: 'profile',
    data: { title: 'Fabio Biondi', color: 'blue', children: 'www.fabiobiondi.com' }
  },
]

const COMPONENTS: { [key: string]: FunctionComponent<any> } = {
  chart: WidgetChart,
  profile: WidgetProfile
}

export function DynamicLayout() {
  const [cfg, setCfg] = useState(dashboard);

  return <div>
    Dynamic
   {
      cfg.map((item, index) => {
        return <Suspense fallback={<div>loading...</div>}>
          {React.createElement(COMPONENTS[item.component], { ...item.data, key: index })}
        </Suspense>
      })
    }
  </div>
}

