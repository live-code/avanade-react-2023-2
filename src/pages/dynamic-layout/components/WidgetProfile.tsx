// components/WidgetProfile.tsx
import React, { PropsWithChildren } from 'react';

export default  function WidgetProfile (props: PropsWithChildren<{ title: string, color: string }>) {
  return <div>
    <h1 style={{ color: props.color}}>{props.title}</h1>
    <div>{props.children}</div>
  </div>
}
