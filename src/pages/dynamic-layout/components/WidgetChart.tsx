// components/WidgetChart.tsx
import React, { PropsWithChildren } from 'react';

export default function WidgetChart (props: { title: string, data: number[] }) {
  return <div className="bg-dark text-white rounded-3">
    <h1>{props.title}</h1>
    {props.data.map((v, idx) => <li key={idx}>{v}</li>)}
  </div>
}
