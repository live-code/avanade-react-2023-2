import { useNavigate } from 'react-router-dom';

export function SignIn() {
  const navigate = useNavigate();

  function loginHandler() {
    localStorage.setItem('islogged', 'true')
    navigate('/products')
  }

  return <div>
    <h1>SignIn</h1>
    <input type="text"/>
    <input type="text"/>
    <button onClick={loginHandler}>SIGN IN</button>

  </div>
};
