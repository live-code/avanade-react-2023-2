import { Product } from '@model/product';
import React from 'react';
import { useCrud } from '../../shared/utils/useCrud';
// import { ProductForm, ProductList, ProductTotal } from './components';
import { ProductForm, ProductList, ProductTotal } from './components';

export default function CMS() {
  const {
    items: products,
    addProduct,
    deleteProduct,
    clear
  } = useCrud<Product>('http://localhost:3001/products');

  return <div className="container">
    <h1>Products</h1>
    {/*<ProductFormUncontrolled onAdd={addHandler}/>*/}
    <ProductForm onAdd={addProduct}/>
    <ProductList
      products={products}
      title="PRODUCT LIST"
      color="red"
      onDelete={deleteProduct}
    />
    <ProductTotal products={products} />
  </div>
}
