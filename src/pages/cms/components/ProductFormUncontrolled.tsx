import React, { useRef } from 'react';
import { Product } from '../../../model/product';



interface ProductFormProps {
  onAdd: (formData: Partial<Product>) => void;
}

export function ProductFormUncontrolled(props: ProductFormProps) {
  const inputEl = useRef<HTMLInputElement | null>(null)
  const inputCostEl = useRef<HTMLInputElement | null>(null)

  function addHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      props.onAdd({
        name: inputEl.current!.value,
        cost: +inputCostEl.current!.value,
      })
    }
  }

  return (
    <div>
      <input onKeyDown={addHandler} type="text" ref={inputEl} />
      <input onKeyDown={addHandler} type="text" ref={inputCostEl} />
    </div>
  )
}
