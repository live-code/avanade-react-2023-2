import React from 'react';
import { Product } from '../../../model/product';

interface ProductTotalProps {
  products: Product[];
}

export function ProductTotal(props: ProductTotalProps) {

  return (
    <div className="card my-3">
      <div className="card-header bg-dark text-white">
        TOTAL: {getTotal(props.products)}
      </div>
    </div>
  )
}


export const getTotal = (products: Product[]) =>
  products.reduce((acc, p) => acc + p.cost, 0);
