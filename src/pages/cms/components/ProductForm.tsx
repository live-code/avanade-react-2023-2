import { Product } from '@model/product';
import clsx from 'clsx';
import React, { useState } from 'react';

interface ProductFormProps {
  onAdd: (formData: Partial<Product>) => void;
}

const initialState = { name: '', cost: 0, desc: ''};

export function ProductForm(props: ProductFormProps) {
  const [formData, setFormData] = useState(initialState);
  const [dirty, setDirty] = useState(false)

  function changeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;
    setFormData(prev => ({ ...prev, [name]: [value]}))
    setDirty(true)
  }

  function save(e: React.FormEvent) {
    e.preventDefault();
    props.onAdd(formData)
  }

  function clearHandler() {
    setFormData(initialState);
    setDirty(false)
  }

  const isNameValid = formData.name.length > 0;
  const isCostValid = formData.cost > 0;
  const isValid = isNameValid && isCostValid;

  return (

    <form onSubmit={save}>
      {isValid && <div>Form is Invalid</div>}


      <input
        type="text"
        className={clsx('form-control', { 'is-invalid': !isNameValid && dirty, 'is-valid': isNameValid })}
        value={formData.name} onChange={changeHandler} placeholder="product name" name="name"/>

      <input
        type="text"
        className={clsx('form-control', { 'is-invalid': !isCostValid && dirty, 'is-valid': isCostValid })}
        value={formData.cost} onChange={changeHandler} placeholder="product cost" name="cost"/>

      <button type="submit" onClick={save} disabled={!isValid}>ADD</button>
      <button type="button" onClick={clearHandler}>Clear</button>
    </form>
  )
}
