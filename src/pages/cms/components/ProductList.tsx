import React, { memo } from 'react';
import { Product } from '../../../model/product';

interface ProductListProps {
  products: Product[];
  title: string;
  color: string;
  onDelete: (idToRemove: number) => void
}

export const ProductList = memo((props: ProductListProps) => {
  return (
    <ul className="list-group">
      <h1 style={{ color: props.color}}>{props.title}</h1>
      {
        props.products.map((p) => (
          <li className="list-group-item d-flex justify-content-between align-items-center" key={p.id}>
            <div> {p.name} ({p.id})</div>
            <div className="d-flex gap-1">
              <div>€{p.cost}</div>
              <div>
                <i className="fa fa-trash"
                   onClick={() => props.onDelete(p.id)} />
              </div>
            </div>
          </li>
        ))
      }

    </ul>
  )
})

