import { useEffect, useRef, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { IfLogged } from '../../core/auth/IfLogged';
import { useCount } from '../../shared/utils/useCount';
import { useResize } from '../../shared/utils/useResize';
import { useProducts } from './hooks/useProducts';

export default function Products() {
  const { count, increment } = useCount(10, 5)
  const { products } = useProducts();
  const ref = useRef(null);

  const size = useResize();


  return <div ref={ref}>
    <h1 onClick={increment}>Home {count}</h1>

    <IfLogged> ADMIN </IfLogged>
    <div>sizes: {size.width} x {size.height}</div>
    {
      products.map(p => (

          <li key={p.id}>
            <NavLink to={`/products/${p.id}`}>
              {p.name}
            </NavLink>
          </li>

      ))
    }

  </div>
}

