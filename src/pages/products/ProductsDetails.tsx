import { Product } from '@model/product';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';

export function ProductsDetails() {
  const obj = useParams<{productId: string}>();
  const [product, setProduct] = useState<Product | null>(null)

  useEffect(() => {
    axios.get(`http://localhost:3001/products/${obj.productId}`)
      .then(res => setProduct(res.data))
  }, [])

  return <div>
    <h1>Scheda prodotto</h1>

    <div>{product?.name}</div>
    <div>{product?.cost}</div>


    <NavLink to="/products">Back to list</NavLink>
  </div>
}
