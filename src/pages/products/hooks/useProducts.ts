import { Product } from '@model/product';
import axios from 'axios';
import { useEffect, useState } from 'react';

export function useProducts() {
  const [ products, setProducts ] = useState<Product[]>([]);

  useEffect(() => {
    axios.get<Product[]>('http://localhost:3001/products')
      .then(res => {
        setProducts(res.data)
      })
  }, []);

  return {
    products
  }
}
