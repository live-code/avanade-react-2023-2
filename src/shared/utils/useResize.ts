import { useEffect, useState } from 'react';


let timer: number;
export function useResize() {
  const [ size, setSize ]= useState({ w: 0, h: 0})

  useEffect(() => {
    const fn =  () => {
      // DEBOUNCE
      clearInterval(timer)
      timer = window.setTimeout(() => {
        setSize({ w: window.innerWidth, h: window.innerHeight});
      }, 1000)
    }
    window.addEventListener('resize', fn)
    return () => window.removeEventListener('resize', fn)
  }, [])

  return {
    width: size.w,
    height: size.h,
  };
}
