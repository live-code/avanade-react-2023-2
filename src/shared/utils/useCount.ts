import { useState } from 'react';

export function useCount(initialValue = 0, step = 1) {
  console.log('useCount')
  const [count, setCount] = useState<number>(initialValue)

  function inc() {
    setCount(s => s + step)
  }
  function dec() {
    setCount(s => s - step)
  }

  return {
    count: count,
    increment: inc,
    decrement: dec,
  }
}
