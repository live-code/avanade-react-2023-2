import axios from 'axios';
import { useCallback, useEffect, useState } from 'react';

export function useCrud<T extends { id: number }>(basePath: string) {
  const [ items, setItems ] = useState<T[]>([]);

  useEffect(() => {
    getAll();
    console.log('use Effect useCrud')
  }, [])

  function getAll() {
    axios.get<T[]>(basePath)
      .then(res => {
        setItems(res.data)
      })
  }

  const deleteHandler = useCallback((idToRemove: number) => {
    axios.delete(`${basePath}/${idToRemove}` )
      .then(() => {
        setItems(
          prev => prev.filter(p => p.id !== idToRemove)
        )
      })
  }, [basePath])

  const addHandler = (formData: Partial<T>) => {
    axios.post<T>(basePath, formData)
      .then(res => {
        setItems(prev => [...prev, res.data])
      })
  }

  function clearHandler() {
    setItems([])
  }

  return {
    items,
    clear: clearHandler,
    addProduct: addHandler,
    deleteProduct: deleteHandler
  }
}
