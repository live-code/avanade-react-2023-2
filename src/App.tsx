import React, { lazy, Suspense } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { Interceptor } from './core/auth/Interceptor';
import { PrivateRoute } from './core/auth/PrivateRoute';
import { NavBar } from './core/NavBar';
import { DynamicLayout } from './pages/dynamic-layout/DynamicLayout';
import { LostPass } from './pages/logi/components/LostPass';
import { Registration } from './pages/logi/components/Registration';
import { SignIn } from './pages/logi/components/SignIn';
import { Login } from './pages/logi/Login';
import { ProductsDetails } from './pages/products/ProductsDetails';

const Products = lazy(() => import('./pages/products/Products') )
const CMS = lazy(() => import('./pages/cms/CMS') )


function App() {

  console.log('render')
  return (
    <div>
        <NavBar />
        <Interceptor />
        <hr/>

        <Routes>
          <Route
            path="login"
            element={ <Login /> }
          >
            <Route path="registration" element={<Registration />} />
            <Route path="lost" element={<LostPass />} />
            <Route path="signin" element={<SignIn />} />
            <Route index element={<Navigate to="/login/signin" />} />
          </Route>




          <Route path="products" element={
            <Suspense fallback={<div>loading</div>}><Products /></Suspense>
          } />


          <Route path="products/:productId" element={ <ProductsDetails />} />

          <Route path="dynamic-layout" element={ <DynamicLayout/>} />

          <Route path="cms" element={
            <Suspense fallback={<div>loading</div>}>
              <PrivateRoute role="admin"><CMS /></PrivateRoute>
            </Suspense>
          } />

          <Route path="/" element={
            <Navigate to="login" />
          } />
          <Route path="*" element={
            <div>Page 404</div>
          } />
        </Routes>
    </div>
  );
}

export default App;


