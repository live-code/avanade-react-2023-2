import React from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { IfLogged } from './auth/IfLogged';

const isActiveHandler = (obj: { isActive: boolean } ) => {
  return obj.isActive ? { color: 'orange'} : {}
};

export function NavBar() {
  const navigate = useNavigate();

  console.log('navbar')

  function logout() {
    localStorage.removeItem('islogged')
    navigate('/login')
  }

  return <div>
    <div className="btn-group">
      <NavLink
        className="btn btn-primary" to="login"
        style={isActiveHandler}
      >Login</NavLink>

      <NavLink
        style={isActiveHandler}
        className="btn btn-primary" to="products">products</NavLink>

      <NavLink
        style={isActiveHandler}
        className="btn btn-primary" to="dynamic-layout">dynamic</NavLink>


      <IfLogged>
        <NavLink
          style={isActiveHandler}
          className="btn btn-primary" to="cms">cms</NavLink>
      </IfLogged>


      <IfLogged>
        <button
          className="btn btn-primary"
          onClick={logout}
        >logout</button>
      </IfLogged>



    </div>
  </div>
}
