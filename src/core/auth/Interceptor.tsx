import { useInterceptor } from './useInterceptor';

export function Interceptor() {
  const { error } = useInterceptor()
  return <div>

    {error && <div className="alert alert-danger">ahia!</div>}
  </div>;
}
