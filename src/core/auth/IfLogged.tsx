import { PropsWithChildren } from 'react';


export function IfLogged(props: PropsWithChildren) {
  return <>
    {
      localStorage.getItem('islogged') ?
        props.children : null
    }
  </>
}

