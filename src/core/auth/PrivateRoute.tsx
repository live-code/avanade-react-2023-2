import { PropsWithChildren } from 'react';


interface PrivateRouteProps {
  role: 'admin' | 'moderator';
}

export function PrivateRoute(props: PropsWithChildren<PrivateRouteProps>) {
  return <div>
    {
      localStorage.getItem('islogged') ?
        props.children : null
    }
  </div>
}
