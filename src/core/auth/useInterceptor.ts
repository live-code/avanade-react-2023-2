import axios from 'axios';
import { useEffect, useState } from 'react';

export function useInterceptor() {
  const [error, setError] = useState(false)
  useEffect(() => {
    console.log('use Effect Interceptor')
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      // Do something before request is sent
      console.log(config.headers.set('authentication', 'orhohoihoihoifho'))
      setError(false)
      return config;
    }, function (error) {
      setError(true)
      // Do something with request error
      return Promise.reject(error);
    });

// Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (error) {
      setError(true)

      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    });
  }, [])

  return { error };
}

